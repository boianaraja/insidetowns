<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Staff_list extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->login_required();
        $this->load->model('admin/Privilege_model');
        $this->load->model('admin/Staff_model');
    }

    function index() {
        $this->check_user_access('staff');
        $this->data['add_access'] = $this->user_access('staff_add');
        $this->data['update_access'] = $this->user_access('staff_update');
        $this->data['page'] = 'staff';
        $this->data['page_unique_name'] = 'staff';
        $this->data['page_title'] = 'Staff List';
        $this->data['list'] = $this->Staff_model->get_staff_list();
        $this->admin_view('staff_list');
    }

    function add_staff() {
        $this->check_user_access('staff_add');
        $this->data['page'] = 'staff';
        $this->data['page_unique_name'] = 'staff';
        $this->data['page_title'] = 'Add Staff';
        $this->data['designation'] = $this->Privilege_model->get_designation();
        $this->admin_view('create_staff');
    }

    function save_staff() {
        $this->check_user_access('staff_add');
        $config = array(
            array(
                'field' => 'phone_number',
                'label' => 'phone_number',
                'rules' => 'required|is_unique[staff.phone_number]',
            ),
            array(
                'field' => 'designation_id',
                'label' => 'Designation Id',
                'rules' => 'required'
            ),
            array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'required'
            ),
            array(
                'field' => 'status',
                'label' => 'Status',
                'rules' => 'required'
            ),
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required|is_unique[staff.email]',
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required'
            ),
        );
        $this->form_validation->set_rules($config);
        if (empty($_FILES['image']['name'])) {
            $this->form_validation->set_rules('image', 'Image', 'required');
        }
        if ($this->form_validation->run() == FALSE) {
            $this->check_user_access('staff_add');
            $this->data['page'] = 'staff';
            $this->data['page_unique_name'] = 'staff';
            $this->data['page_title'] = 'Add Staff';
            $this->data['designation'] = $this->Privilege_model->get_designation();
            $this->admin_view('create_staff');
        } else {
            $image_name = $this->file_upload('image');
            $salt = randomPassword(5);
            $password = md5($this->input->get_post('password') . $salt);
            $data = array(
                'designation_id' => $this->input->get_post('designation_id'),
                'name' => $this->input->get_post('name'),
                'email' => $this->input->get_post('email'),
                'phone_number' => $this->input->get_post('phone_number'),
                'password' => $password,
                'salt' => $salt,
                'image' => $image_name,
                'status' => $this->input->get_post('status'),
                'created_date' => CURRENT_DATE_TIME
            );
            $result = $this->Staff_model->save_staff($data);
            if ($result) {
                $this->session->set_flashdata('success_message', '"Staff Added Successfully","Success"');
                redirect(base_url() . 'admin/staff_list');
            } else {
                $this->session->set_flashdata('error_message', '"Please try again later.","Failed!"');
                redirect(base_url() . 'admin/staff_list');
            }
        }
    }

    function edit_staff($staff_id) {
        $this->data['details'] = $this->common_model->get_single_data('staff', $staff_id);
        $this->check_user_access('staff_update');
        if ($this->input->post('submit')) {
            $config = array(
                array(
                    'field' => 'phone_number',
                    'label' => 'phone_number',
                    'rules' => 'callback_phone_number',
                ),
                array(
                    'field' => 'designation_id',
                    'label' => 'Designation Id',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'name',
                    'label' => 'Name',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'status',
                    'label' => 'Status',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'email',
                    'label' => 'Email',
                    'rules' => 'callback_email',
                )
            );
            $this->form_validation->set_rules($config);
            if ($_FILES['image']['name'] == '') {
                $this->form_validation->set_rules('image1', 'Old Image', 'required');
                $image_name = $this->input->get_post('image1');
            } else {
                $image_name = $this->file_upload('image');
            }
            if ($this->form_validation->run() == FALSE) {
                $this->data['page'] = 'staff';
                $this->data['page_unique_name'] = 'staff';
                $this->data['page_title'] = 'Add Staff';
                $this->data['designation'] = $this->Privilege_model->get_designation();
                $this->admin_view('create_staff');
            } else {
                $data1 = array(
                    'designation_id' => $this->input->get_post('designation_id'),
                    'name' => $this->input->get_post('name'),
                    'email' => $this->input->get_post('email'),
                    'phone_number' => $this->input->get_post('phone_number'),
                    'image' => $image_name,
                    'status' => $this->input->get_post('status'),
                    'created_date' => CURRENT_DATE_TIME
                );
                if ($this->input->get_post('password') != '') {
                    $salt = randomPassword(5);
                    $password = md5($this->input->get_post('password') . $salt);
                    $data1['password'] = $password;
                    $data1['salt'] = $salt;
                }
                $result = $this->Staff_model->update_staff($data1, $staff_id);
                if ($result) {
                    $this->session->set_flashdata('success_message', '"Staff Added Successfully","Success"');
                    redirect(base_url() . 'admin/staff_list');
                } else {
                    $this->session->set_flashdata('error_message', '"Please try again later.","Failed!"');
                    redirect(base_url() . 'admin/staff_list');
                }
            }
        } else {
            $this->data['page'] = 'staff';
            $this->data['page_unique_name'] = 'staff';
            $this->data['page_title'] = 'Add Staff';
            $this->data['designation'] = $this->Privilege_model->get_designation();
            $this->admin_view('create_staff');
        }
    }

    public function phone_number($str) {
        if ($this->data['details']->phone_number == $str) {
            return TRUE;
        } else {
            $sql = "SELECT COUNT(id) as no_of_rows FROM staff WHERE phone_number = '" . trim($this->input->post('phone_number')) . "'";
            $count = $this->db->query($sql)->row()->no_of_rows;
            if ($count > 0) {
                $this->form_validation->set_message('phone_number', 'phone number must be unique');
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }

    public function email($str) {
        if ($this->data['details']->email == $str) {
            return TRUE;
        } else {
            $sql = "SELECT COUNT(id) as no_of_rows FROM staff WHERE email = '" . trim($this->input->post('email')) . "'";
            $count = $this->db->query($sql)->row()->no_of_rows;
            if ($count > 0) {
                $this->form_validation->set_message('email', 'email must be unique');
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }

}
