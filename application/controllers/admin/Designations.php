<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Designations extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->login_required();
        $this->load->model('admin/Designations_model');
    }

    public function index() {
        $this->check_user_access('designations');
        $this->data['add_access'] = $this->user_access('designations_add');
        $this->data['update_access'] = $this->user_access('designations_update');
        $this->data['check_access'] = $this->data['add_access'];
        $this->data['page'] = 'Privileges';
        $this->data['page_unique_name'] = 'designations';
        $this->data['page_title'] = 'Designations';
        $this->data['list'] = $this->Designations_model->get_data();
        $this->admin_view('designations');
    }

    function add_designations() {
        $this->check_user_access('relations_add');
        $config = array(
            array(
                'field' => 'title',
                'label' => 'title',
                'rules' => 'required|is_unique[designation.title]',
            ),
//            array(
//                'field' => 'description',
//                'label' => 'Description',
//                'rules' => 'required',
//            ),
            array(
                'field' => 'status',
                'label' => 'Status',
                'rules' => 'required'
            )
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->check_user_access('designations');
            $this->data['add_access'] = $this->user_access('designations_add');
            $this->data['update_access'] = $this->user_access('designations_update');
            $this->data['check_access'] = $this->data['add_access'];
            $this->data['page'] = 'Privileges';
            $this->data['page_unique_name'] = 'designations';
            $this->data['page_title'] = 'Designations';
            $this->data['list'] = $this->Designations_model->get_data();
            $this->admin_view('designations');
        } else {
            $insert_data = array(
                'title' => trim($this->input->post('title')),
                'description' => trim($this->input->post('description')),
                'status' => $this->input->post('status'),
                'created_date' => date('Y-m-d H:i:s')
            );
            $result = $this->common_model->add_data('designation', $insert_data);
            if ($result) {
                $this->session->set_flashdata('success_message', '"Designation Added Successfully","Success"');
                redirect(base_url() . 'admin/designations');
            } else {
                $this->session->set_flashdata('error_message', '"Please try again later.","Failed!"');
                redirect(base_url() . 'admin/designations');
            }
        }
    }

    function edit_designations($id) {
        $this->check_user_access('designations_update');
        $this->data['details'] = $this->common_model->get_single_data('designation', $id);
        if ($this->input->post('submit')) {
            $config = array(
                array(
                    'field' => 'title',
                    'label' => 'title',
                    'rules' => 'callback_title',
                ),
                array(
                    'field' => 'status',
                    'label' => 'Status',
                    'rules' => 'required'
                )
            );
            $this->form_validation->set_rules($config);

            if ($this->form_validation->run() == FALSE) {
                $this->check_user_access('designations');
                $this->data['add_access'] = $this->user_access('designations_add');
                $this->data['update_access'] = $this->user_access('designations_update');
                $this->data['check_access'] = $this->data['update_access'];
                $this->data['page'] = 'Privileges';
                $this->data['page_unique_name'] = 'designations';
                $this->data['page_title'] = 'Designations';
                $this->data['list'] = $this->Designations_model->get_data();
                $this->admin_view('designations');
            } else {
                $insert_data = array(
                    'title' => trim($this->input->post('title')),
                    'description' => trim($this->input->post('description')),
                    'status' => $this->input->post('status'),
                    'updated_date' => date('Y-m-d H:i:s')
                );
                $result = $this->common_model->update('designation', $insert_data, $id);
                if ($result) {
                    $this->session->set_flashdata('success_message', '"Designation Updated Successfully","Success"');
                    redirect(base_url() . 'admin/designations');
                } else {
                    $this->session->set_flashdata('error_message', '"Please try again later.","Failed!"');
                    redirect(base_url() . 'admin/designations');
                }
            }
        } else {
            $this->check_user_access('designations');
            $this->data['add_access'] = $this->user_access('designations_add');
            $this->data['update_access'] = $this->user_access('designations_update');
            $this->data['check_access'] = $this->data['update_access'];
            $this->data['page'] = 'Privileges';
            $this->data['page_unique_name'] = 'designations';
            $this->data['page_title'] = 'Designations';
            $this->data['list'] = $this->Designations_model->get_data();
            $this->admin_view('designations');
        }
    }

    public function title($str) {
        if ($this->data['details']->title == $str) {
            return TRUE;
        } else {
            $sql = "SELECT COUNT(title) as no_of_rows FROM designation WHERE title = '" . trim($this->input->post('title')) . "'";
            $count = $this->db->query($sql)->row()->no_of_rows;
            if ($count > 0) {
                $this->form_validation->set_message('title', 'Designation/Role must be unique');
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }

}
