<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Terms_conditions extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->login_required();
        $this->load->model('admin/Terms_conditions_model');
    }

    public function index() {
        $this->check_user_access('terms_and_conditions');
        $this->data['update_access'] = $this->user_access('terms_and_conditions_update');
        $this->data['check_access'] = $this->data['update_access'];
        $this->data['page'] = 'CMS';
        $this->data['page_unique_name'] = 'terms_and_conditions';
        $this->data['page_title'] = 'Terms and Conditions';
        $this->data['details'] = $this->Terms_conditions_model->get_data();
        $this->admin_view('terms_conditions');
    }

    function edit() {
        $this->check_user_access('terms_and_conditions_update');
        if(trim(strip_tags($this->input->post('terms_conditions'))) == ''){
            $this->session->set_flashdata('warning_message', '"Terms and Conditions cannot be empty.","Failed!"');
            redirect(base_url() . 'admin/terms_conditions');
        }
        $result = $this->Terms_conditions_model->edit();
        if ($result) {
            $this->session->set_flashdata('success_message', '"Terms and Conditions Updated Successfully","Success"');
            redirect(base_url() . 'admin/terms_conditions');
        } else {
            $this->session->set_flashdata('error_message', '"Please try again later.","Failed!"');
            redirect(base_url() . 'admin/terms_conditions');
        }
    }

}
