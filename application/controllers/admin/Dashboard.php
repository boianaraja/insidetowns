<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->login_required();
        $this->load->model('admin/Dashboard_model');
    }

    public function index() {
        $this->data['page'] = 'dashboard';
        $this->data['page_unique_name'] = 'dashboard';
        $this->data['page_title'] = 'Dashboard';

        $this->admin_view('dashboard');
    }

}
