<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->login_required();
    }

    // ------------- Active/Inactive For all ------------------
    function deactive($id, $table) {
        $result = $this->common_model->deactive($table, $id);
        if ($result) {
            $this->session->set_flashdata('success_message', '"Status Changed Successfully","Success"');
            echo '<script>window.history.go(-1)</script>';
        } else {
            $this->session->set_flashdata('error_message', '"Please try again later.","Failed!"');
            echo '<script>window.history.go(-1)</script>';
        }
    }

    // ------------- Delete Data From DB ------------------
    function delete_data($id, $table) {
        $result = $this->common_model->delete($table, $id);
        if ($result) {
            $this->session->set_flashdata('success_message', '"Data Deleted Successfully","Success"');
            echo '<script>window.history.go(-1)</script>';
        } else {
            $this->session->set_flashdata('error_message', '"Please try again later.","Failed!"');
            echo '<script>window.history.go(-1)</script>';
        }
    }
    
    function error_page(){
        $this->load->view('admin/error_page');
    }
}
