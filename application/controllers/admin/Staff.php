<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Staff extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/Staff_login_model');
    }

    public function index() {
        $this->load->view('admin/staff');
    }

    function login() {
        if ($this->input->post('login')) {
            $result = $this->Staff_login_model->get_staff_details();
//            echo $this->db->last_query();die;
            if ($result) {
                $password = md5($this->input->post('password') . $result->salt);
                if ($result->password == $password) {
                    $data = array(
                        'staff_id' => $result->id,
                        'ip_address' => $this->input->ip_address(),
                        'created_date' => date('Y-m-d H:i:s')
                    );
                    $logs = $this->common_model->add_data('staff_logs', $data);
                    if ($logs) {
                        $data = array(
                            'user_name' => $result->name,
                            'user_id' => $result->id,
                            'validated' => TRUE,
                            'user_type' => 'staff',
                            'log_id' => $logs,
                            'designation_id' => $result->designation_id,
                            'image' => $result->image
                        );
                        $this->session->set_userdata($data);
//                        print_r($this->session->userdata());die;
                        $this->session->set_flashdata('success_message', '"Welcome ' . $result->name . '","Success"');
                        redirect(base_url() . 'admin/dashboard');
                    } else {
                        $this->session->set_flashdata('error_message', '"Please try again","Failed!"');
                        redirect(base_url() . 'staff');
                    }
                } else {
                    $this->session->set_flashdata('error_message', '"Password not matched","Failed!"');
                    redirect(base_url() . 'staff');
                }
            } else {
                $this->session->set_flashdata('error_message', '"Username not found","Failed!"');

                redirect(base_url() . 'staff');
            }
        }
    }

    public function logout() {
        $data = array(
            'updated_date' => date('Y-m-d H:i:s')
        );
        $res = $this->common_model->update('staff_logs', $data, $this->session->userdata('log_id'));
        if ($res) {
            $this->session->sess_destroy();
            $this->session->set_flashdata('success_message', '"Logged Out Successsfully","Success"');
            redirect(base_url() . 'staff');
        } else {
            $this->session->set_flashdata('error_message', '"please try again later","Failed!"');
            redirect(base_url() . 'admin/dashboard');
        }
    }

    public function forgot_password() {
        if ($this->input->get_post('submit')) {
            $res = $this->Staff_login_model->get_staff_details();
            if ($res) {
                $salt = rand(10000, 99999);
                $rand_pwd = randomPassword(6);
                $password = $rand_pwd . $salt;
                $data = array(
                    'salt' => $salt,
                    'password' => md5($password),
                    'updated_date' => date('Y-m-d H:i:s')
                );
                $result = $this->common_model->update('staff', $data, $res->id);
                if ($result) {
                    $subject = "Twinlity Staff Panel Forgot password";
                    $message = "Hello " . $res->name . ", New password is: " . $rand_pwd . ".\nReset your password to secure your account.";
                    if ($res->email != '') {
                        $this->send_mail($subject, $message, $res->email);
                    }
                    if ($res->phone_number != '') {
                        send_sms($res->phone_number, $message);
                    }
                    $this->session->set_flashdata('success_message', '"Password sent Successsfully","Success"');
                    redirect(base_url() . 'staff');
                } else {
                    $this->session->set_flashdata('error_message', '"Please try again","Failed!"');
                    redirect(base_url() . 'staff');
                }
            } else {
                $this->session->set_flashdata('error_message', '"Username not found","Failed!"');
                redirect(base_url() . 'admin');
            }
        }
        $this->load->view('admin/staff_forgot_password');
    }

}
