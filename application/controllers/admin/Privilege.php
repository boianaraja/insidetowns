<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Privilege extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->login_required();
        $this->load->model('admin/Privilege_model');
    }

    public function index() {
        $this->check_user_access('privileges');
        $this->data['update_access'] = $this->user_access('privileges_update');
        $this->data['check_access'] = $this->data['update_access'];
        $this->data['page_unique_name'] = 'privileges';
        $this->data['page'] = 'Privileges';
        $this->data['page_title'] = 'Privilege';
        $this->data['designation'] = $this->Privilege_model->get_designation();
        //$this->data['privilege'] = $this->Privilege_model->get_privilege();
        $this->admin_view('privilege');
    }
    
    public function get_previliges($designation) {
        $this->check_user_access('privileges');
        $this->data['update_access'] = $this->user_access('privileges_update');
        $this->data['check_access'] = $this->data['update_access'];
        $this->data['page_unique_name'] = 'privileges';
        $this->data['page'] = 'Privileges';
        $this->data['page_title'] = 'Privilege';
        $this->data['designation_id'] = $designation;
        $this->data['designation'] = $this->Privilege_model->get_designation();
        $this->data['privilege'] = $this->Privilege_model->get_privilege($designation);
        $this->admin_view('privilege');
    }

    public function add_privileges() {
        $this->check_user_access('privileges_update');
//        echo "<pre>";
//        print_r($_POST);
//        die;
        if (count($this->input->post('privilege')) == 0) {
            $this->session->set_flashdata('warning_message', '"Please select atleast one privilige.","Warning!"');
            redirect(base_url() . 'admin/privilege');
        }
        
        if ($this->input->post('designation') == '') {
            $this->session->set_flashdata('warning_message', '"Please select designation.","Warning!"');
            redirect(base_url() . 'admin/privilege');
        }
        
//        if ($this->input->post('designation') == 1) {
//            $this->session->set_flashdata('warning_message', '"Please check designation name.","Warning!"');
//            redirect(base_url() . 'admin/privilege');
//        }
        
        $result = $this->Privilege_model->add_privileges();
        if ($result) {
            $this->session->set_flashdata('success_message', '"privilege Updated Successfully","Success"');
            redirect(base_url() . 'admin/privilege');
        } else {
            $this->session->set_flashdata('error_message', '"Please try again later.","Failed!"');
            redirect(base_url() . 'admin/privilege');
        }
    }

}
