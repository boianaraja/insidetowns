<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Privacy_policy extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->login_required();
        $this->load->model('admin/Privacy_policy_model');
    }

    public function index() {
        $this->check_user_access('privacy_policy');
        $this->data['update_access'] = $this->user_access('privacy_policy_update');
        $this->data['check_access'] = $this->data['update_access'];
        $this->data['page'] = 'CMS';
        $this->data['page_unique_name'] = 'privacy_policy';
        $this->data['page_title'] = 'Privacy Policy';
        $this->data['details'] = $this->Privacy_policy_model->get_data();
        $this->admin_view('privacy_policy');
    }
    
    function edit(){
        $this->check_user_access('privacy_policy_update');
        if(trim(strip_tags($this->input->post('privacy_policy'))) == ''){
            $this->session->set_flashdata('warning_message', '"Privacy Policy cannot be empty.","Failed!"');
            redirect(base_url() . 'admin/privacy_policy');
        }
        $result = $this->Privacy_policy_model->edit();
        if ($result) {
            $this->session->set_flashdata('success_message', '"Privacy Policy Updated Successfully","Success"');
            redirect(base_url() . 'admin/Privacy_policy');
        } else {
            $this->session->set_flashdata('error_message', '"Please try again later.","Failed!"');
            redirect(base_url() . 'admin/Privacy_policy');
        }
    }
}
