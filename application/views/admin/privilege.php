<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?= $page_title; ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= base_url(); ?>/dashboard">Dashboard</a>
            </li>
            <li class="active">
                <strong><?= $page_title; ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-md-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $page_title; ?></h5>
                </div>
                <div class="ibox-content">
                    <form method="post" class="form-horizontal" autocomplete="off" action="<?= base_url(); ?>admin/Privilege/add_privileges" enctype="multipart/form-data">
                        <div class="col-md-4">
                            <div class="form-group"><label class="col-sm-12">Designation</label>
                                <div class="col-md-12">
                                    <select class="form-control" name="designation" required="" onchange="refresh_page(this.value)">
                                        <option value="">Select Designation</option>
                                        <?php foreach ($designation as $row) { ?>
                                            <option value="<?= $row->id; ?>" <?php
                                            if (isset($designation_id) && $designation_id == $row->id) {
                                                echo 'selected';
                                            }
                                            ?> ><?= strtoupper($row->title); ?></option>
                                                <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8"></div>
                        <div class="col-sm-12">
                            <!--<pre><?= print_r($privilege); ?></pre>-->
                            <?php if (isset($designation_id)) { ?>
                            <p style="color: red; text-transform: capitalize"><strong>Note:</strong> For Add, Update, Delete or Any other functionality you must need to give view access.</p>
                                <table class="table table-bordered">
                                    <tr>
                                        <th>Menu Title</th>
                                        <th><input type="checkbox" id="checkall" /> <label for='checkall'>Privilege</label></th>
                                    </tr>
                                    <tbody>
                                        <?php foreach ($privilege as $row) { ?>
                                            <tr>
                                                <th colspan="2">
                                                    <?= $row->name; ?>
                                                </th>
                                            </tr>

                                            <?php
                                            foreach ($row->privileges_items as $item) {
                                                ?>
                                                <tr>
                                                    <th style="text-align: right">
                                                        <?= strtoupper($item->type); ?> -- <?= strtoupper($item->unique_name); ?>
                                                    </th>
                                                    <td>
                                                        <input type="checkbox" class="checkall" value="<?= $item->id; ?>" name="privilege[]" <?php if ($item->checked == 1) echo 'checked'; ?> />
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            <?php if($check_access == 1) { ?>
                                <div class="col-md-2 col-md-offset-5">
                                    <input type="submit" value="Save Changes" name="submit" class="btn btn-primary" />
                                </div>
                            <?php } ?>
                            <?php } ?>
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <?php
    $this->load->view('admin/includes/footer');
    ?>
    <script>
        $('#checkall').on('click', function () {
            $("input:checkbox").prop('checked', $(this).prop("checked"));
        });
        function refresh_page(designation) {
            if (designation != '') {
                window.location.href = "<?= base_url(); ?>admin/privilege/get_previliges/" + designation;
            } else {
                window.location.href = "<?= base_url(); ?>admin/privilege";
            }
        }
    </script>