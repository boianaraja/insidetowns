<div class="row">
    <div class="">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= $page_title; ?></h5>
                <?php if ($add_access == 1) { ?>
                    <a href="<?= base_url(); ?>admin/staff_list/add_staff" style="margin-top: -8px;" class="btn btn-info btn-md pull-right"><i class="fa fa-plus"></i> Add Staff</a>
                <?php } ?>
            </div>
            <div class="ibox-content">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Image</th>
                                <th>Employee Name</th>
                                <th>Phone Number</th>
                                <th>Email</th>
                                <th>Designation</th>
                                <th>Date & Time</th>
                                <th>Status</th>
                                <th>Manage</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($this->input->get_post('per_page')) {
                                $s = $this->input->get_post('per_page');
                            } else {
                                $s = 0;
                            }
                            foreach ($list as $row) {
                                ?>

                                <tr>
                                    <td><?= ++$s; ?></td>
                                    <td><img src="<?= FILE_PATH . $row->image; ?>" class="img-thumbnail" style="width: 45px;height: 45px;"/></td>
                                    <td><?= $row->name; ?></td>
                                    <td><?= $row->phone_number; ?></td>
                                    <td><?= $row->email; ?></td>
                                    <td><?= $row->designation; ?></td>
                                    <td><?= date('d M Y h:i A', strtotime($row->created_date)); ?></td>
                                    <td><?php if ($row->status == 1) { ?>
                                            <button value="<?= $row->id ?>" class="btn btn-xs btn-primary change_status">Active</button>
                                        <?php } else { ?>
                                            <button value="<?= $row->id ?>" class="btn btn-xs btn-danger change_status">Inactive</button>
                                        <?php } ?></td>
                                    <td>
                                        <a href="<?= base_url(); ?>admin/staff_list/edit_staff/<?= $row->id; ?>" class="btn btn-success btn-xs"><i class="fa fa-edit"></i> Edit</a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <!--<div class="text-center"><?= $pagination; ?></div>-->
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$this->load->view('admin/includes/footer');
?>
<script>
    $(function () {
        $('.datepicker').datepicker({
            dateFormat: 'yy-mm-d',
            changeMonth: true,
            changeYear: true,
            maxDate: 0
        });
    });
</script>