<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>INSPINIA | 404 Error</title>

    <link href="<?= base_url(); ?>admin_assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= base_url(); ?>admin_assets/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?= base_url(); ?>admin_assets/css/animate.css" rel="stylesheet">
    <link href="<?= base_url(); ?>admin_assets/css/style.css" rel="stylesheet">

</head>

<body class="gray-bg">


    <div class="middle-box text-center animated fadeInDown">
        <h1><i class="text-danger fa fa-ban"></i></h1>
        <h3 class="font-bold">Access Denied</h3>

        <div class="error-desc">
            For any queries  please contact support team<br/><a href="<?=base_url();?>admin/dashboard" class="btn btn-primary m-t">Go Back To Dashboard</a>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="<?= base_url(); ?>admin_assets/js/jquery-2.1.1.js"></script>
    <script src="<?= base_url(); ?>admin_assets/js/bootstrap.min.js"></script>

</body>

</html>
