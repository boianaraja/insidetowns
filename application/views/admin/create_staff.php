<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?= $page_title; ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= base_url(); ?>/dashboard">Dashboard</a>
            </li>
            <li class="active">
                <strong><?= $page_title; ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $page_title; ?></h5>
                    <a href="<?= base_url(); ?>admin/staff_list" style="margin-top: -8px;" class="btn btn-info btn-md pull-right"><i class="fa fa-arrow-left"></i> Back</a>
                </div>
                <div class="ibox-content">
                    <form method="post" class="form-horizontal" autocomplete="off" action="<?= base_url(); ?><?php if (!isset($details)) { ?>admin/staff_list/save_staff<?php } else { ?>admin/staff_list/edit_staff/<?= $details->id; ?><?php } ?>" enctype="multipart/form-data">
                        <div class="col-md-4">
                            <div class="form-group"><label class="col-sm-12">Designation</label>
                                <div class="col-sm-12">
                                    <select class="form-control" name="designation_id">
                                        <option value="">Select Designation</option>
                                        <?php foreach ($designation as $row) { ?>
                                            <option value="<?= $row->id; ?>" <?php
                                            if (isset($details->designation_id) && $details->designation_id == $row->id) {
                                                echo 'selected';
                                            }
                                            ?> ><?= strtoupper($row->title); ?></option>
                                                <?php } ?>
                                    </select>
                                    <?php echo form_error('designation_id', '<div class="error">', '</div>'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group"><label class="col-sm-12">Name</label>
                                <div class="col-sm-12">
                                    <input type="text" placeholder="Name" name="name" class="form-control" value="<?php if (isset($details)) echo $details->name; ?>" required="" >
                                    <?php echo form_error('name', '<div class="error">', '</div>'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group"><label class="col-sm-12">Email</label>
                                <div class="col-sm-12">
                                    <input type="email" placeholder="Email" name="email" class="form-control" value="<?php if (isset($details)) echo $details->email; ?>" required="" >
                                    <?php echo form_error('email', '<div class="error">', '</div>'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group"><label class="col-sm-12">Phone Number</label>
                                <div class="col-sm-12">
                                    <input type="text" maxlength="10" placeholder="Phone Number" name="phone_number" class="form-control number" value="<?php if (isset($details)) echo $details->phone_number; ?>" required="" >
                                    <?php echo form_error('phone_number', '<div class="error">', '</div>'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group"><label class="col-sm-12">Password</label>
                                <div class="col-sm-10" style="padding-right: 0;">
                                    <input type="password" placeholder="Password" name="password" id="password" class="form-control" <?php if (!isset($details)) { ?>required="" <?php } ?> style="border-right-width: 0px">
                                    <?php echo form_error('password', '<div class="error">', '</div>'); ?>
                                </div>
                                <div class="col-lg-2" style="padding-left: 0;">
                                    <button type="button" class="btn btn-md btn-white" onclick="show_password('password', 'password_btn')" style="border-top-left-radius: 0;border-bottom-left-radius: 0;border-left: 0;"><i class="fa fa-eye" id="password_btn"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group"><label class="col-sm-12">Image</label>
                                <?php if (isset($details)) { ?>
                                    <div class="col-sm-4">
                                        <img src="<?= FILE_PATH . $details->image; ?>" class="img-thumbnail" />
                                        <input type="hidden" value="<?= $details->image; ?>" name="image1"/>
                                        <input type="hidden" value="<?= $details->id; ?>" name="id"/>
                                    </div>
                                <?php } ?>
                                <div class="<?php
                                if (!isset($details)) {
                                    echo 'col-sm-12';
                                } else {
                                    echo 'col-sm-8';
                                }
                                ?>">
                                    <input type="file" name="image" id="file" accept="image/x-png,image/jpeg" onchange="check_file_size(this.id)" class="form-control" <?php if (!isset($details) || $details->image == '') echo 'required=""'; ?> >
                                    <?php echo form_error('image', '<div class="error">', '</div>'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-sm-12">Status</label>
                                <div class="col-sm-6">
                                    <div class="radio radio-success">
                                        <input type="radio" name="status" id="radio1" value="1" required="" <?php if (isset($details) && $details->status == 1) echo 'checked="checked"'; ?>>
                                        <label for="radio1">
                                            <b>Active</b>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="radio radio-danger">
                                        <input type="radio" name="status" id="radio2" value="0" <?php if (isset($details) && $details->status == 0) echo 'checked="checked"'; ?>>
                                        <label for="radio2">
                                            <b>Inactive</b>
                                        </label>
                                    </div>
                                </div>
                                <?php echo form_error('status', '<div class="error">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-4 col-md-offset-4">
                            <input type="submit" name="submit" class="btn btn-primary btn-block" value="Save">
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$this->load->view('admin/includes/footer');
?>
<script>
    function show_password(filed_id, tag_id) {
        $('#' + tag_id).toggleClass("fa-eye fa-eye-slash");
        input = $('#' + filed_id);
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    }
</script>