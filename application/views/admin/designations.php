<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?= $page_title; ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= base_url(); ?>/dashboard">Dashboard</a>
            </li>
            <li class="active">
                <strong><?= $page_title; ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-md-8">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $page_title; ?></h5>
                </div>
                <div class="ibox-content">
                    <!-- table-responsive -->
                    <div class="">
                        <table class="table table-striped table-bordered table-hover dataTables" >
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Designation</th>
                                    <th style="width: 40%">Description</th>
                                    <th>Status</th>
                                    <?php if ($update_access == 1) { ?>
                                        <th>Manage</th>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $s = 0;
                                foreach ($list as $row) {
                                    ?>
                                    <tr>
                                        <td><?= ++$s; ?></td>
                                        <td><?= $row->title; ?></td>
                                        <td><?= $row->description; ?></td>
                                        <td><?php if ($row->status == 1) { ?>
                                                <button value="<?= $row->id ?>" class="btn btn-xs btn-primary <?php if ($update_access == 1) echo 'change_status'; ?>">Active</button>
                                            <?php } else { ?>
                                                <button value="<?= $row->id ?>" class="btn btn-xs btn-danger <?php if ($update_access == 1) echo 'change_status'; ?>">Inactive</button>
                                            <?php } ?>
                                        </td>
                                        <?php if ($update_access == 1) { ?>
                                            <td>
                                                <a href="<?= base_url(); ?>admin/designations/edit_designations/<?= $row->id ?>" class="btn btn-success btn-xs"><i class="fa fa-edit"></i> Edit</a>
                                                <!--<button type="button" class="btn btn-xs btn-danger delete_item" value="<?= $row->id ?>"><i class="fa fa-trash"></i> Delete</button>-->
                                            </td>
                                        <?php } ?>
                                    </tr>
                                <?php }  ?>
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <?php if ($check_access == 1) { ?>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?php
                            if (isset($details)) {
                                echo "Edit";
                            } else {
                                echo "Add";
                            }
                            ?> <?= $page_title; ?></h5>
                    </div>
                    <div class="ibox-content">
                        <form method="post" class="form-horizontal" autocomplete="off" action="<?= base_url(); ?><?php if (!isset($details)) { ?>admin/designations/add_designations<?php } else { ?>admin/designations/edit_designations/<?= $details->id; ?><?php } ?>" enctype="multipart/form-data">
                            <div class="col-md-12">
                                <div class="form-group"><label class="col-sm-12">Designation</label>
                                    <div class="col-sm-12">
                                        <input type="text" placeholder="Designation/Role" name="title" class="form-control" value="<?php if (isset($details)) echo $details->title; ?>" required="" >
                                        <?php echo form_error('title', '<div class="error">', '</div>'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group"><label class="col-sm-12">Description</label>
                                    <div class="col-sm-12">
                                        <textarea placeholder="Description" maxlength="950" name="description" class="form-control"><?php if (isset($details)) echo $details->description; ?></textarea>
                                        <?php echo form_error('description', '<div class="error">', '</div>'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-12">Status</label>
                                    <div class="col-sm-6">
                                        <div class="radio radio-success">
                                            <input type="radio" name="status" id="radio1" value="1" required="" <?php if (isset($details) && $details->status == 1) echo 'checked="checked"'; ?>>
                                            <label for="radio1">
                                                <b>Active</b>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="radio radio-danger">
                                            <input type="radio" name="status" id="radio2" value="0" <?php if (isset($details) && $details->status == 0) echo 'checked="checked"'; ?>>
                                            <label for="radio2">
                                                <b>Inactive</b>
                                            </label>
                                        </div>
                                    </div>
                                    <?php echo form_error('status', '<div class="error">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="col-md-4 col-md-offset-4">
                                <input type="submit" name="submit" class="btn btn-primary btn-block" value="Save">
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<?php
$this->load->view('admin/includes/footer');
?>
<script type="text/javascript">
    $(document).on("click", '.change_status', function () {
        var designation_id = $(this).val();
        swal({
            title: "Are you sure?",
            text: "You want to Change the Status!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, change it!",
            closeOnConfirm: false
        },
        function () {
            window.location = "<?= base_url() ?>admin/common/deactive/" + designation_id + "/designation";
        });
    });
</script>