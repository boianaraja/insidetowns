<!--<style>
.ck-editor__editable_inline {
    min-height: 400px;
}
</style>-->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?= $page_title; ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= base_url(); ?>/dashboard">Dashboard</a>
            </li>
            <li class="active">
                <strong><?= $page_title; ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-md-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $page_title; ?></h5>
                </div>
                <div class="ibox-content">
                    <form method="post" class="form-horizontal" autocomplete="off" action="<?= base_url(); ?>admin/terms_conditions/edit" enctype="multipart/form-data">
                        <div class="col-md-12">
                            <div class="form-group"><label class="col-sm-12">Qualification</label>
                                <div class="col-sm-12">
                                    <textarea id="editor" rows="100" cols="100" class="form-control" name="terms_conditions"><?php if (isset($details)) echo $details->terms_conditions; ?></textarea>
                                </div>
                            </div>
                        </div>
                        <?php if($check_access == 1){ ?>
                        <div class="col-md-4 col-md-offset-4">
                            <input type="submit" name="submit" class="btn btn-primary btn-block" value="Save">
                        </div>
                        <?php } ?>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$this->load->view('admin/includes/footer');
?>
<script>
    CKEDITOR.replace('terms_conditions', {
        height: 500
    });
//    ClassicEditor.create(document.querySelector('#editor'), {
//    // toolbar: [ 'heading', '|', 'bold', 'italic', 'link' ]
//    });
</script>