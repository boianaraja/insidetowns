<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Staff_model extends CI_Model {

    function save_staff($data) {
        $this->db->set($data);
        if ($this->db->insert('staff')) {
            return TRUE;
        }
        return FALSE;
    }

    function get_staff_list() {
        $this->db->select('staff.*, designation.title as designation');
        $this->db->join('designation','staff.designation_id = designation.id');
        $this->db->order_by('staff.id', 'desc');
        return $this->db->get('staff')->result();
    }
    
    function update_staff($data, $id) {
        $this->db->set($data);
        $this->db->where('id', $id);
        if ($this->db->update('staff')) {
            return TRUE;
        }
        return FALSE;
    }
    
}
