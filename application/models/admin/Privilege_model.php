<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Privilege_model extends CI_Model {

    function get_designation() {
        return $this->db->where('title !=', 'admin')->order_by('id', 'DESC')->get('designation')->result();
    }

    function get_privilege($designation) {
        $privilege = $this->db->select('name, unique_name_for_query')->where('type', 'view')->where('status', 1)->order_by('priority', 'ASC')->get('privilege')->result();
        foreach ($privilege as $row) {
            $row->privileges_items = $this->db->where('unique_name_for_query', $row->unique_name_for_query)->order_by('display_order_by', 'ASC')->get('privilege')->result();
            foreach ($row->privileges_items as $item) {
                $item->checked = $this->db->where('privilege_id', $item->id)->where('designation', $designation)->limit(1)->get('access_privilege')->num_rows();
            }
        }
        return $privilege;
    }

    function add_privileges() {
        $this->db->trans_begin();
        $priviliges = $this->input->post('privilege');
        $designation = $this->input->post('designation');
        $this->db->where('designation', $designation)->delete('access_privilege');
        foreach ($priviliges as $row) {
            $this->db->set('designation', $designation);
            $this->db->set('privilege_id', $row);
            $this->db->set('created_date', date('Y-m-d H:i:s'));
            $this->db->insert('access_privilege');
        }
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

}
