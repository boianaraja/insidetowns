<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Designations_model extends CI_Model {

    function get_data() {
        return $this->db->where('title !=', 'admin')->order_by('id', 'DESC')->get('designation')->result();
    }

}
