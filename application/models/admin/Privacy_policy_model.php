<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Privacy_policy_model extends CI_Model {

    public function get_data() {
        return $this->db->where('id', 1)->limit(1)->get('privacy_policy')->row();
    }

    public function edit() {
        $this->db->set('privacy_policy', $this->input->post('privacy_policy'));
        $this->db->set('updated_date', date('Y-m-d H:i:s'));
        $this->db->where('id', 1);
        $this->db->update('privacy_policy');
        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
