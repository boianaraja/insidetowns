<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Terms_conditions_model extends CI_Model {

    public function get_data() {
        return $this->db->where('id', 1)->limit(1)->get('terms_conditions')->row();
    }

    public function edit() {
        $this->db->set('terms_conditions', $this->input->post('terms_conditions'));
        $this->db->set('updated_date', date('Y-m-d H:i:s'));
        $this->db->where('id', 1);
        $this->db->update('terms_conditions');
        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
