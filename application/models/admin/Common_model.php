<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Common_model extends CI_Model {

    function admin_details() {
        $admin_id = $this->session->userdata('user_id');
        $this->db->where('id', $admin_id);
        $this->db->limit(1);
        return $this->db->get('admin')->row();
    }

    function staff_details() {
        $staff = $this->session->userdata('user_id');
        $this->db->where('id', $staff);
        $this->db->limit(1);
        return $this->db->get('staff')->row();
    }

    function update($table_name, $data, $updated_id) {
        $this->db->set($data);
        $this->db->where('id', $updated_id);
        $this->db->update($table_name);
        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function add_data($table_name, $data) {
        $this->db->set($data);
        $this->db->insert($table_name);
        $last_id = $this->db->insert_id();
        if ($last_id) {
            return $last_id;
        } else {
            return FALSE;
        }
    }

    public function deactive($table, $id) {
        if ($this->db->where('id', $id)->get($table)->row()->status == 1) {
            $this->db->set('status', 0);
        } else {
            $this->db->set('status', 1);
        }
        $this->db->set('updated_date', date('Y-m-d H:i:s'));
        $this->db->where('id', $id);
        if ($this->db->update($table)) {
            //echo $this->db->last_query();die;
            return true;
        } else {
            return false;
        }
    }

    public function delete($table, $id) {
        $this->db->where('id', $id);
        if ($this->db->delete($table)) {
            return true;
        } else {
            return false;
        }
    }

    public function get_single_data($table, $id) {
        $this->db->where('id', $id);
        $res = $this->db->get($table);
        if ($res->num_rows() > 0) {
            return $res->row();
        } else {
            return false;
        }
    }

    public function get_order_by_priority_data($table) {
        return $this->db->order_by('priority', 'ASC')->get($table)->result();
    }

    function get_all_records_by_desc_order($table_name) {
        return $this->db->order_by('id', 'DESC')->get($table_name)->result();
    }

}
